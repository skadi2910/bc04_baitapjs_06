const changeBgColor_btn = document.getElementById("changeBG");
const bodyMain = document.getElementById("main");
const bgValue = document.getElementById("bg-value");
changeBgColor_btn.addEventListener("click", changeBgColor);

function changeBgColor() {
  const bgMainColor = generate_random_hex_color();
  bodyMain.style.backgroundColor = bgMainColor;
  bodyMain.style.transition = `1s`;
  bgValue.value = `${bgMainColor}`;
}
function changeBgColor_gradient() {
  const bgColor_1 = generate_random_hex_color();
  const bgColor_2 = generate_random_hex_color();
  bodyMain.style.background = `linear-gradient(${90}deg,${bgColor_1},${bgColor_2})`;
  bgValue.value = `${bgColor_1} ${bgColor_2}`;
}
const generate_random_hex_color = () => {
  const maxVal = 0xffffff; // 16777215
  let random_number = Math.random() * maxVal;
  random_number = Math.floor(random_number);
  let random_color = random_number.toString(16);
  return `#${random_color}`;
};
