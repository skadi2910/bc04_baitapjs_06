// CÂU 1
function timSoChanLe(num) {
  const oddsArr = [];
  const evensArr = [];
  const resultsArr = [];
  for (let n = 0; n < num; n++) {
    if (n % 2 === 0) {
      evensArr.push(n);
    } else {
      oddsArr.push(n);
    }
  }
  resultsArr[0] = evensArr;
  resultsArr[1] = oddsArr;
  return resultsArr;
}
function inSoChanLe() {
  const numVal = document.getElementById("txt-num-1").value * 1;
  const results = timSoChanLe(numVal);
  document.getElementById("result_1").innerHTML = `
  <p>Chẵn: ${results[0]}</p> 
  <p>Lẻ: ${results[1]}</p>`;
}
// CÂU 2
function timSoNguyenDuong(numVal) {
  let sum = 0;
  for (let n = 0; sum < numVal; n++) {
    sum += n;
    if (sum >= numVal) {
      return n;
    }
  }
}
function inSoNguyenDuong() {
  const numVal = document.getElementById("txt-num-2").value * 1;
  const result = timSoNguyenDuong(numVal);
  document.getElementById("result_2").innerHTML = `
  Số nguyên dương nhỏ nhất: 
  ${result}`;
}
// CÂU 3
function tinhTong(x, n) {
  let sum = 0;
  for (let i = 1; i <= n; i++) {
    sum += Math.pow(x, i);
  }
  return sum;
}
function inTong() {
  const numX_Val = document.getElementById("txt-num-3_x").value * 1;
  const numN_Val = document.getElementById("txt-num-3_n").value * 1;
  const result = tinhTong(numX_Val, numN_Val);
  document.getElementById("result_3").innerHTML = `Tổng: ${result} `;
}
// CÂU 4
function tinhGiaiThua(inputVal) {
  let num = 1;
  for (let i = 1; i <= inputVal; i++) {
    num *= i;
  }
  return num;
}
function inGiaiThua() {
  const inputVal = document.getElementById("txt-num-4").value * 1;
  const result = tinhGiaiThua(inputVal);
  document.getElementById("result_4").innerHTML = `!${inputVal} = ${result}`;
}
// CÂU 5

/* ********** Create Div Element **************
function themTheDivTheoSoLuong(num) {
  const content = new DocumentFragment();
  for (let i = 1; i <= num; i++) {
    const div_red = document.createElement("div");
    div_red.classList.add("text-white", "text-center", "bg-danger", "py-2");
    const div_blue = document.createElement("div");
    div_blue.classList.add("text-white", "text-center", "bg-primary", "py-2");
    if (i % 2 === 0) {
      div_blue.innerText = `Div chẵn ${i}`;
      content.append(div_blue);
    } else {
      div_red.innerText = `Div lẻ ${i}`;
      content.append(div_red);
    }
  }
  return content;
}*/

/*String ****/
function themTheDiv(num) {
  let content = "";
  for (let i = 1; i <= num; i++) {
    if (i % 2 === 0) {
      const div_blue = `<div class="text-white text-center bg-primary py-2">Div chẵn ${i}</div>`;
      content += div_blue;
    } else {
      const div_red = `<div class="text-white text-center bg-danger py-2">Div lẻ ${i}</div>`;
      content += div_red;
    }
  }
  return content;
}
function hienThiTheDiv() {
  document.getElementById("result_5").innerHTML = ""; // Clear content
  const numOfDiv = document.getElementById("txt-num-div").value * 1;
  document.getElementById("result_5").innerHTML = themTheDiv(numOfDiv);
}
// CÂU 6
function isPrime(num) {
  if (num < 2 || Number.isInteger(num) == false) {
    return false;
  } else {
    for (let x = 2; x < num; x++) {
      if (num % x === 0) {
        return false;
      }
    }
    return true;
  }
}
function inSoNguyenTo() {
  const range = document.getElementById("txt-num-6").value * 1;
  const results = [];
  for (let i = 0; i <= range; i++) {
    if (isPrime(i) === true) {
      results.push(i);
    }
  }
  document.getElementById("result_6").innerHTML = `${results}`;
}
